---
title: H3ll0 Blender
date: "2017-12-28"
---

Hello World in 3D for Blender with a render out in VR acceptable.

1. [Download blender and BlendVR](http://blender-vr-manual.readthedocs.io/installation/installation-manual.html)
1. Open your first document and learn the basics of the interface:
<iframe width="560" height="315"
src="https://www.youtube.com/embed/kes2qmijy7w" frameborder="0"
gesture="media" allow="encrypted-media" allowfullscreen></iframe>
1. Create the text
<div style="position:relative;height:0;padding-bottom:56.25%"><iframe
src="https://www.youtube.com/embed/74pUyg2LWro?ecver=2" width="640"
height="360" frameborder="0" gesture="media" allow="encrypted-media"
style="position:absolute;width:100%;height:100%;left:0"
allowfullscreen></iframe></div>
1. Add in the world. 
You can
[search](https://www.blendswap.com/blends/search?keywords=earth&is_fan_art=1&blend_license=&render_engine=&sort=edit_date&direction=desc)
for earth models at [blendswap](https://www.blendswap.com/)
I am going to use [this](https://www.blendswap.com/blends/view/22444)
one. We'll start by importing the model and then replacing the dot
under our exclamation point with our earth model.
1. Add in the camera orbit.
[Example stack exchange question about circlular motion](https://blender.stackexchange.com/questions/3476/how-can-i-animate-the-camera-in-a-perfect-circular-rotation-around-a-fixed-posit)
1. Render.
[Example stack exchange question about blender rendering for G cardboard](https://blender.stackexchange.com/questions/39190/rendering-3d-video-image-for-google-cardboard)

And your first hello world in 3D has been done. Enjoy!

Now go and try some more blender [tutorials](https://www.blender.org/support/tutorials/).

And, also, [cloud.blender.com](https://cloud.blender.org/welcome) is
another official place from the blender team where they have a great collection
of tutorials.

There is also a [great place for getting models](https://www.blendswap.com/).
